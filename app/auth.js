firebase.initializeApp({
    apiKey: 'AIzaSyBrlb_1bluC1vbIuvxYD7QkTTC2pZvNCKE',
    authDomain: 'usuarios-8f665.firebaseapp.com',
    projectId: 'usuarios-8f665'
});

var db = firebase.firestore();
const auth = firebase.auth();

const signWithGoogle = ()=>{
	const provider = new firebase.auth.GoogleAuthProvider();

	auth.signInWithPopup(provider)
	.then(()=>{
		// console.log("ez")
	})
	.catch(error=>{
		console.log(error)
	})

}

function login(email,pass){

    auth.signInWithEmailAndPassword(email,pass)
    .then(userCredential => {
        window.location.assign('app.html')
    })
    .catch(function(error) {
        Swal.fire({
            title: 'Upps!',
            text: 'Credenciales incorrectas',
            icon: 'error',
            confirmButtonText: 'Reintentar'
        })
    });
}

function loginGoogle(e){
	signWithGoogle();
}

function logout(){
    auth.signOut().then(()=>{
        localStorage.removeItem('userEmail')
        localStorage.removeItem('UserdisplayName')
        localStorage.removeItem('UserphotoURL')
    })
}