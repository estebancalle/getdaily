function getRandomToken(){
    var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789";
    var token = "";
    for (i=0; i<20; i++) token +=caracteres.charAt(Math.floor(Math.random()*caracteres.length)); 
    return token;
}

function quitTask(id) {

    Swal.fire({
        title: 'Está seguro de eliminar esta tarea?',
        text: "Este cambio no tiene reverso",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
    if (result.isConfirmed) {

        db.collection("tasks").doc(id).delete()
        .then(function(){
            // Swal.fire({
            //     title: 'Genial!',
            //     text: 'Ticket Eliminado',
            //     icon: 'success',
            //     confirmButtonText: 'Aceptar'
            // })
        })
        .catch(function(error){
            Swal.fire({
                title: 'Upps!',
                text: 'Hubo un error al actualizar al eliminar el ticket'+error,
                icon: 'error',
                confirmButtonText: 'Reintentar'
            })
        })  
    }
    })
}

// Función que marca la fecha de la tarea al ser finalizada
function checkTaskwithDate(){

    var element = db.collection("tasks").doc($("#status-id").val());

    $("#modalCheckTask").modal("hide")

    if ($("#check-date").val()==='') {

        Swal.fire({
            title: 'Upps!',
            text: 'Debes indicar una fecha',
            icon: 'error',
            confirmButtonText: 'Aceptar'
        })

       
    }else{

        Swal.fire({
            title: 'Genial!',
            text: 'Fecha Actualizada',
            icon: 'success',
            confirmButtonText: 'Aceptar'
        })

        return element.update({
            taskStatus:$("#status-data").val(),
            DueDate:moment($("#check-date").val(),"YYYY-MM-DD").format('YYYY-MM-DD')
        })

        
    }
    
}

function checkTask(id,status){

    var element = db.collection("tasks").doc(id);

    // Varaiable que controla cuando pedir la fecha
    let checkDate = true;

    if (status=='1') {
        status = '0';
        checkDate = false
    }else{
        status = '1';
    }

    $("#status-data").val(status)
    $("#status-id").val(id)


    Swal.fire({
        title: 'Desea marcar ésta tarea?',
        text: "Por favor diga la fecha de finalización de ésta tarea a continuación",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'marcar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
    if (result.isConfirmed) {
        
        

        if (checkDate===true) {
            $("#modalCheckTask").modal("show")
        }else{

            Swal.fire({
                title: 'Genial!',
                text: 'Estado actualizado',
                icon: 'success',
                confirmButtonText: 'Aceptar'
            })
    

            return element.update({
                taskStatus:status,
                DueDate:moment().format('0000-00-00')
            })
        }

        

        // .then(function() {
        //     // Swal.fire({
        //     //     title: 'Genial!',
        //     //     text: 'Ticket Actualizado',
        //     //     icon: 'success',
        //     //     confirmButtonText: 'Aceptar'
        //     // })
    
        //     // $("#EditStatus").modal('hide');
    
        // })
        // .catch(function(error) {
        //     Swal.fire({
        //         title: 'Upps!',
        //         text: 'Hubo un error al actualizar el estado de la tarea a contibnuación',
        //         icon: 'error',
        //         confirmButtonText: 'Reintentar'
        //     })
        // });
    }
    })
}

function quitTag(id,idTag){
    Swal.fire({
        title: 'Está seguro de eliminar ésta etiqueta?',
        text: "Este cambio no tiene reverso",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
    if (result.isConfirmed) {

        // console.log(idTag);        

        // Consultamos las tareas asociadas a esa etiqueta
        db.collection("tasks").where('taskTag', '==', idTag).onSnapshot((querySnapshot) => {
            console.log("a");
            querySnapshot.forEach((doc) => {
                // RECORREMOS TODAS Y LE CAMBIAMOS EL idTag por ''
                console.log(doc.data());

                // var db = firebase.firestore();
                var element = db.collection("tasks").doc(doc.id);

                return element.update({
                    taskTag:''
                })
                .then(function() {
                   
                    // luego ahí si eliminamos la etiqueta
                    db.collection("tags").doc(id).delete()
                    .then(function(){
                        Swal.fire({
                            title: 'Genial!',
                            text: 'Etiqueta Eliminada',
                            icon: 'success',
                            confirmButtonText: 'Aceptar'
                        })
                    })
                    .catch(function(error){
                        Swal.fire({
                            title: 'Upps!',
                            text: 'Hubo un error al actualizar al eliminar la etiqueta'+error,
                            icon: 'error',
                            confirmButtonText: 'Reintentar'
                        })
                    })  

                })
                .catch(function(error) {
                    Swal.fire({
                        title: 'Upps!',
                        text: 'Hubo un error al actualizar los datos de la etiqueta',
                        icon: 'error',
                        confirmButtonText: 'Reintentar'
                    })
                });

            });
            
        })

    }
    })
}

function putFieldsEditTag(id,nombre,color){

    $("#tagNameEdit").val(nombre);
    $("#tagColorEdit").val(color);
    $("#tagIdEdit").val(id);

}


async function getDataFB(el){
    const snapshot = await db.collection('tags').where('tagId', '==', el).get()
    return snapshot.docs.map(doc => doc.data());
}

function editTask(taskName, date, id, taskTag){
    
    $("#editTaskName").val(taskName)
    $("#editOptionalDate").val(date)
    $("#idEdit").val(id)


    // Cargamos el select con las etiquetas
    db.collection("tags").where('idUser', '==', localStorage.getItem("userEmail")).onSnapshot((querySnapshot) => {

        if (!querySnapshot.empty) {
            let e2 = $("#SelectTags2").empty();
            e2.append('<option>Seleccionar Etiqueta</option>')
            querySnapshot.forEach((doc) => {
                if (taskTag===doc.data().tagId) {
                    e2.append('<option selected value="'+doc.data().tagId+'">'+doc.data().tagName+'</option>')
                }else{
                    e2.append('<option value="'+doc.data().tagId+'">'+doc.data().tagName+'</option>')
                }
                
            });
        //  $("#totalPending").html(count)
        }else{
            // $("#totalPending").html(0)
        }
    })

    $("#ModalEdit").modal("show")
}

function editTaskAction(){

    let id = $("#idEdit").val()
    let taskName = $("#editTaskName").val()
    let taskDate = moment($("#editOptionalDate").val(),"YYYY-MM-DD").format('YYYY-MM-DD')
    let taskTag = $("#SelectTags2 option:selected").val()

    var element = db.collection("tasks").doc(id);

    Swal.fire({
        title: 'Genial!',
        text: 'Tarea Actualizada',
        icon: 'success',
        confirmButtonText: 'Aceptar'
    })

    return element.update({
        taskName:taskName,
        taskDate:taskDate,
        taskTag:taskTag
    })

}

// function getDailyQuoete(){
//     axios.get('https://type.fit/api/quotes')
//     .then(function (response) {
//         // handle success

//         let array=[];

//         response.data.forEach(data=>{
//             array.push(data);
//         })

//         const aleatorio = array[Math.floor(Math.random() * array.length)];
//         console.log(aleatorio)

//     })
//     .catch(function (error) {
//         // handle error
//         // console.log(error);
//     })
//     .then(function () {
//         // always executed
//     });
// }
		
