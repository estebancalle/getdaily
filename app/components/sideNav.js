new Vue({
	el:'#sideNav',

	data(){
		return{
			
		}
	},

	template:`
		<nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="nav-profile-image">
                  <img src="${localStorage.getItem('UserphotoURL')}" alt="profile">
                  <span class="login-status online"></span>
                  <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column">
                  <span class="font-weight-bold mb-2">${localStorage.getItem('UserdisplayName')}</span>
                  <span class="text-secondary text-small"></span>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="app.html">
                <span class="menu-title">Inicio</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tags.html">
                <span class="menu-title">Etiquetas</span>
                <i class="mdi mdi-tag menu-icon"></i>
              </a>
            </li>
            <li class="nav-item sidebar-actions">
              <span class="nav-link">
                <button class="btn btn-block btn-lg btn-gradient-info mt-4" data-toggle="modal" data-target="#myModal">GETDAILY!</button>
              </span>
            </li>
          </ul>
        </nav>
	`


})