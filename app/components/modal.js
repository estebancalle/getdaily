new Vue({
	el:'#modal',

	data(){
		return{
			fechaAusencia:''
		}
	},

	mounted() {
		this.getDeily()
	},

	methods: {
		searchLost(){

			if (this.fechaAusencia=='') {
				Swal.fire({
					title: 'Upps!',
					text: 'Debes de indicar la fecha',
					icon: 'error',
					confirmButtonText: 'Reintentar'
				})
			}

			let temp = moment(this.fechaAusencia,"YYYY-MM-DD").format('YYYY-MM-DD')

			let col = document.getElementById('ausentDay');

            db.collection("tasks").where('idUser', '==', localStorage.getItem("userEmail")).where('DueDate', '==', temp).onSnapshot((querySnapshot)=>{

				$("#ausentDay").empty()

				if (querySnapshot.docs=='') {
					Swal.fire({
						title: 'Upps!',
						text: 'No hayamos tareas para '+temp,
						icon: 'error',
						confirmButtonText: 'Reintentar'
					})
				}

                querySnapshot.forEach((doc)=>{

					let completed = '';
					let checked = '';

					if (doc.data().taskStatus=='1') {
						completed = 'completed';
						checked = 'checked';
					}

					if (doc.data().taskStatus=='1') {
						col.innerHTML+=`
							<li class="${completed}">
								<div class='form-check'><label class='form-check-label'>
								<input class='checkbox' type='checkbox' ${checked} />${doc.data().taskName}
									<i class='input-helper'></i></label>
								</div>
							</li>
                    	`
					}

                    
                })
            })
		},

		getDeily(){

			let  yesterday = moment().subtract(1, "days").format("YYYY-MM-DD");
			let  today = moment().format("YYYY-MM-DD");

			let col1 = document.getElementById('DailyYerterday');
			let col2 = document.getElementById('DailyToday');


			// YESTERDAY
            db.collection("tasks").where('idUser', '==', localStorage.getItem("userEmail")).where('taskStatus', '==', '1').onSnapshot((querySnapshot)=>{

				$("#DailyYerterday").empty()

                querySnapshot.forEach((doc)=>{

					let completed = '';
					let checked = '';


					if (doc.data().taskDate==yesterday) {
						if (doc.data().taskStatus=='1') {
							completed = 'completed';
							checked = 'checked';
						}
	
						col1.innerHTML+=`
							<li class="${completed}">
								<div class='form-check'><label class='form-check-label'>
								<input class='checkbox' type='checkbox' ${checked} />${doc.data().taskName}
									<i class='input-helper'></i></label>
								</div>
							</li>
						`
					}
                })
            })


			// TODAY
			db.collection("tasks").where('idUser', '==', localStorage.getItem("userEmail")).where('taskStatus', '==', '0').onSnapshot((querySnapshot)=>{

				$("#DailyToday").empty()

                querySnapshot.forEach((doc)=>{

					let completed = '';
					let checked = '';
					let overdue ='';
					let special = '';

					if (doc.data().taskStatus=='1') {
						completed = 'completed';
						checked = 'checked';
					}

					// let tempDateTask = doc.data().taskDate.split('/').reverse().join('');
					let tempDateTask = doc.data().taskDate;

					// Evaluamos si está atrasada
					if (tempDateTask < today) {
						overdue = '(atrasada '+doc.data().taskDate+')';
						special = 'color:#F75C4C';
					}

					if (doc.data().specialTask && doc.data().taskDate>=today) {
						special = 'color:#D4AF37';
					}

					if (tempDateTask <= today) {

						// Mostramos las tareas con etiqueta
						async function showTaskWithTags(){
							let f = await getDataFB(doc.data().taskTag);
							let tagIdentifier = '';

							if (!f[0].idUser=='') {
								tagIdentifier = `<label class="badge badge-gradient" style="color:white;background-color:${f[0].tagColor}">${f[0].tagName}</label>`;											
							}

							col2.innerHTML+=`
							<li class="${completed}">
								<div class='form-check'><label class='form-check-label' style="${special}">
								<input class='checkbox' type='checkbox' ${checked} />${doc.data().taskName + overdue} 
									<i class='input-helper'></i>${tagIdentifier}</label>
								</div>
							</li>
						`
						}

						// Invocamos la función
						showTaskWithTags()


						if (doc.data().taskTag=='') {
							col2.innerHTML+=`
							<li class="${completed}">
								<div class='form-check'><label class='form-check-label' style="${special}">
								<input class='checkbox' type='checkbox' ${checked} />${doc.data().taskName + overdue} 
									<i class='input-helper'></i></label>
								</div>
							</li>
						`
						}

						
					}

                    
                })
            })

		}
		
	},

	template:`
		<div id="myModal" class="modal fade bd-example-modal-lg" role="dialog">
	        <div class="modal-dialog">
	          <!-- Modal content-->
	          <div class="modal-content">
	            <div class="modal-header">
	              <h4 class="modal-title">Daily</h4>
	            </div>
	            <div class="modal-body">
					<div class="alert alert-info" role="alert"><strong>TIP</strong> Si es lunes, puedes consultar lo que hiciste el día viernes</div>

					<form class="form-inline" v-on:submit.prevent="searchLost">
						<div class="form-group mx-sm-3 mb-2">
							<input type="date" class="form-control" v-model="fechaAusencia">
						</div>
						<button type="submit" class="btn btn-primary mb-2">Buscar tareas</button>
					</form>
					
					<ul class="d-flex flex-column-reverse todo-list todo-list-custom" id="ausentDay">
						
					</ul>
					<hr>

					<h5>Para el día de ayer realicé las siguientes tareas:</h5>
					<ul class="d-flex flex-column-reverse todo-list todo-list-custom" id="DailyYerterday">
						<li class="completed">
							<div class='form-check'><label class='form-check-label'>
								<input class='checkbox' type='checkbox' checked/>Prueba
								<i class='input-helper'></i></label>
							</div>
						</li> 
					</ul>

					<hr>
					<h5>Para hoy tengo:</h5>
					<ul class="d-flex flex-column-reverse todo-list todo-list-custom" id="DailyToday">
						<li class="completed">
							<div class='form-check'><label class='form-check-label'>
								<input class='checkbox' type='checkbox'/>Prueba
								<i class='input-helper'></i></label>
							</div>
						</li> 
					</ul>
					
	            </div>
	            <div class="modal-footer">
	              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	            </div>
	          </div>
	        </div>
	    </div>
	`
})