new Vue({
	el:'#tagsContent',

	data(){
		return{
			tagName:'',
            tagColor:'',
			tagNameEdit:'',
            tagColorEdit:'',
			tagId:0
		}
	},

	mounted() {
		this.getTagsOnTable()
	},

	methods:{

		getTagsOnTable(){
			var dataSet = [];

			var table = $("#tablaEtiquetas").DataTable({
				lengthMenu: [[10 ,20, 50, -1], [10, 20, 50, 'todos']],
			})

			db.collection("tags").where("idUser", '==', localStorage.getItem("userEmail")).onSnapshot((querySnapshot)=>{

				table.clear()

                querySnapshot.forEach((doc)=>{


					let estado = '';

					let optionsButton = `
					<div class="dropdown">
						<button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Acciones
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="j#" data-toggle="modal" data-target="#modalEditarEtiqueta" onclick="putFieldsEditTag('${doc.id}','${doc.data().tagName}','${doc.data().tagColor}')">Editar</a>
							<a class="dropdown-item" href="javascript:void(0)" onclick="quitTag('${doc.id}','${doc.data().tagId}')">Eliminar</a>
						</div>
					</div>
					`

					tagIdentifier = `<label class="badge badge-gradient" style="color:white;background-color:${doc.data().tagColor}">${doc.data().tagName}</label>`;

					dataSet = [tagIdentifier, optionsButton]

					table.rows.add([dataSet]).draw()
                })
	
            })
		},

		Savetag(){

			let continuar= true;

			if (this.tagName=='') {
				Swal.fire({
					title: 'Upps!',
					text: 'Debes de poner el nombre de la etiqueta',
					icon: 'error',
					confirmButtonText: 'Reintentar'
				})

				continuar = false;
			}

			// Validamos si no hay color, entonces ponemos negro por defecto
			if (this.tagColor=='') {
				this.tagColor='black';
			}

			if(continuar){

				// Inhabilitamos botón
				$("#add-tag").prop('disabled',true);
				$("#add-tag").html('procesando');

				// Guardamos en FB
				db.collection("tags").add({
					idUser: localStorage.getItem('userEmail'),
					tagName:this.tagName,
					tagColor:this.tagColor,
					tagId:getRandomToken()
				})
				.then(function(docRef) {
				Swal.fire({
					title: 'Genial!',
					text: 'Etiqueta Guardada',
					icon: 'success',
					confirmButtonText: 'Aceptar'
				})
				
				// Habilitamos botón
				$("#add-tag").prop('disabled',false);
				$("#add-tag").html('Crear');

				})
				.catch(function(error) {
				Swal.fire({
					title: 'Upps!',
					text: 'Hubo un error al general la etiqueta',
					icon: 'error',
					confirmButtonText: 'Reintentar'
				})
				//   $("#exampleModal").modal('hide')
				//   enabledButton('SaveButton')
				});

				this.optionalDate=''
				this.specialTask=false
				this.taskName = ''
			}
		},

		EditTag(){

			let id = $("#tagIdEdit").val();
			let nombre = $("#tagNameEdit").val();
			let color = $("#tagColorEdit").val();

			// var db = firebase.firestore();
			var element = db.collection("tags").doc(id);

			return element.update({
				tagName:nombre,
				tagColor:color
			})
			.then(function() {
				Swal.fire({
					title: 'Genial!',
					text: 'Etiqueta Actualizada',
					icon: 'success',
					confirmButtonText: 'Aceptar'
				})

				$("#modalEditarEtiqueta").modal('hide');

			})
			.catch(function(error) {
				Swal.fire({
					title: 'Upps!',
					text: 'Hubo un error al editar la etiqueta',
					icon: 'error',
					confirmButtonText: 'Reintentar'
				})
			});

		}
	},

	template:`
		<div>

			<div class="page-header">
	          <h3 class="page-title">
	            <span class="page-title-icon bg-gradient-info text-white mr-2">
	              <i class="mdi mdi-tag"></i>
	            </span> Etiquetas
	          </h3>
	        </div>

	        <div class="row">
	          <div class="col-md-12 grid-margin stretch-card">
	            <div class="card">
	                <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 mx-auto">
                                <div>
                                    <input type="text" class="form-control todo-list-input" placeholder="Nombre de la etiqueta" v-model="tagName">
                                </div>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <div>
                                    <input type="color" class="form-control todo-list-input" placeholder="¡Cuéntame que harás!" v-model="tagColor">
                                </div>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <div>
                                    <button class="add btn btn-block btn-gradient-info font-weight-bold todo-list-add-btn" id="add-tag" v-on:click="Savetag">Crear</button>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
	          </div>
	        </div>
	        
	        <div class="row">
	          <div class="col-12 grid-margin">
	            <div class="card">
	              <div class="card-body">
	                <h4 class="card-title">Mis Etiquetas</h4>
	                <div class="table-responsive">
	                  <table class="table" id="tablaEtiquetas">
	                    <thead>
	                      <tr>
	                        <th>Etiqueta</th>
	                        <th>Acción</th>
	                      </tr>
	                    </thead>
	                    <tbody id="body-table-tags">
	                      
	                    </tbody>
	                  </table>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>

			<div id="modalEditarEtiqueta" class="modal fade bd-example-modal-lg" role="dialog">
				<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
					<h4 class="modal-title">Editar Etiqueta</h4>
					</div>
					<div class="modal-body">						
						<div class="row">
                            <div class="col-md-4 mx-auto">
                                <div>
                                    <input type="text" class="form-control todo-list-input" placeholder="Nombre de la etiqueta" id="tagNameEdit">
                                </div>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <div>
                                    <input type="color" class="form-control todo-list-input" placeholder="¡Cuéntame que harás!" id="tagColorEdit">
                                </div>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <div>
									<input type="hidden" id="tagIdEdit" v-model="tagId">
                                    <button class="add btn btn-block btn-gradient-info font-weight-bold todo-list-add-btn" v-on:click="EditTag">Guardar cambios</button>
                                </div>
                            </div>
                        </div>
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
				</div>
			</div>
		</div>
	`
})