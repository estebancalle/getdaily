new Vue({
	el:'#dashContent',

	data(){
		return{
			taskName:'',
			taskDate:'',
			taskStatus:'',
			optionalDate:'',
			specialTask:false,
			taskTag:''
		}
	},

	mounted() {
		this.getTask()
		this.getTaskOnTable()
		this.getStats()
		this.getSpecialTask()
		this.getTagsOnSelect()
	},

	methods:{

		getStats(){

			// Pendientes
			db.collection("tasks").where('taskStatus', '==', "0").where('idUser', '==', localStorage.getItem("userEmail")).onSnapshot((querySnapshot) => {

				if (!querySnapshot.empty) {
				let count = 0;
				 querySnapshot.forEach((doc) => {
					count++;
				 });
				 $("#totalPending").html(count)
				}else{
					$("#totalPending").html(0)
				}
			})

			// Realizadas
			db.collection("tasks").where('taskStatus', '==', "1").where('idUser', '==', localStorage.getItem("userEmail")).onSnapshot((querySnapshot) => {

				if (!querySnapshot.empty) {
				let count = 0;
				 querySnapshot.forEach((doc) => {
					count++;
				 });
				 $("#totalDone").html(count)
				}else{
					$("#totalDone").html(0)
				}
			})
		},

		getTask(){
			let col = document.getElementById('taskList');

			let today = moment().format('YYYY-MM-DD');

            db.collection("tasks").where('idUser', '==', localStorage.getItem("userEmail")).where('taskStatus', '==', '0').onSnapshot((querySnapshot)=>{

				$("#taskList").empty()

                querySnapshot.forEach((doc)=>{

					let completed = '';
					let checked = '';
					let overdue ='';
					let special = '';

					if (doc.data().taskStatus=='1') {
						completed = 'completed';
						checked = 'checked';
					}

					// Evaluamos si está atrasada
					if (doc.data().taskDate<today) {
						overdue = '(atrasada '+doc.data().taskDate+')';
						special = 'color:#F75C4C';
					}

					if (doc.data().specialTask && doc.data().taskDate>=today) {
						special = 'color:#D4AF37';
					}


					// mostramos solo las que están pendiente a partir de hoy hacia atrás
					if (doc.data().taskDate<=today) {

						// Mostramos las tareas con etiqueta
						async function showTaskWithTags(){
							try{
								let f = await getDataFB(doc.data().taskTag);
								let tagIdentifier = '';

								if (!f[0].idUser=='') {
									tagIdentifier = `<label class="badge badge-gradient" style="color:white;background-color:${f[0].tagColor}">${f[0].tagName}</label>`;											
								}

								col.innerHTML+=`
								<li class="${completed}">
									<div class='form-check'><label class='form-check-label' style="${special}">
										<input class='checkbox' type='checkbox' onclick="checkTask('${doc.id}','${doc.data().taskStatus}')"  ${checked} />${doc.data().taskName + overdue}
										<i class='input-helper'></i>${tagIdentifier}</label>
									</div>
									<i class='remove mdi mdi-close-circle-outline' onclick="quitTask('${doc.id}')"></i>
								</li>
								`
							}catch{

							}
						}


						// invocamos la función
						showTaskWithTags()


						// Mostramos las tareas sin etiquetas
						if (doc.data().taskTag=='') {
							col.innerHTML+=`
							<li class="${completed}">
								<div class='form-check'><label class='form-check-label' style="${special}">
									<input class='checkbox' type='checkbox' onclick="checkTask('${doc.id}','${doc.data().taskStatus}')"  ${checked} />${doc.data().taskName + overdue}
									<i class='input-helper'></i></label>
								</div>
								<i class='remove mdi mdi-close-circle-outline' onclick="quitTask('${doc.id}')"></i>
							</li>
							`
						}

						
					}

                    
                })
            })
		},

		getTaskOnTable(){
			var dataSet = [];

			var table = $("#tablaTareas").DataTable({
				lengthMenu: [[10 ,20, 50, -1], [10, 20, 50, 'todos']],
				responsive: true
			})

			db.collection("tasks").where("idUser", '==', localStorage.getItem("userEmail")).onSnapshot((querySnapshot)=>{

				table.clear()

                querySnapshot.forEach((doc)=>{


					let estado = '';

					let optionsButton = `
					<div class="dropdown">
						<button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Acciones
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="javascript:void(0)" onclick="checkTask('${doc.id}','${doc.data().taskStatus}')">Cambiar estado</a>
							<a class="dropdown-item" href="javascript:void(0)" onclick="quitTask('${doc.id}')">Eliminar</a>
							<a class="dropdown-item" href="javascript:void(0)" onclick="editTask('${doc.data().taskName}','${doc.data().taskDate}', '${doc.id}', '${doc.data().taskTag}')">Editar</a>
						</div>
					</div>
					`

					if(doc.data().taskStatus=='0') {
						estado = `<label class="badge badge-gradient-info">Pendiente</label>`;
					}else{
						estado = `<label class="badge badge-gradient-success">Finalizada</label>`;
					}

					let dueDate = '0000-00-00';

					if (doc.data().hasOwnProperty('DueDate')) {
						dueDate = doc.data().DueDate
					}

					// Object.prototype.hasOwnProperty.call(obj, 'key')

					dataSet = [doc.data().taskName, estado, doc.data().taskDate, dueDate , optionsButton]

					table.rows.add([dataSet]).draw()
                })
	
            })
		},

		addTask(){

			let continuar= true;

			if (this.taskName=='') {
				Swal.fire({
					title: 'Upps!',
					text: 'Debes de poner el nombre de la tarea',
					icon: 'error',
					confirmButtonText: 'Reintentar'
				})

				continuar = false;
			}

			let fecha = '';

			if (!this.optionalDate=='') {
				fecha = moment(this.optionalDate,"YYYY-MM-DD").format('YYYY-MM-DD')
			}else{
				fecha = moment().format('YYYY-MM-DD')
			}

			if (this.taskTag=='') {
				this.taskTag=0;
			}

			if(continuar){
				// Guardamos en FB
				db.collection("tasks").add({
					idUser: localStorage.getItem('userEmail'),
					taskName:this.taskName,
					taskDate:fecha,
					DueDate:'0000-00-00',
					taskStatus:'0',
					specialTask:this.specialTask,
					taskTag:this.taskTag
				})
				.then(function(docRef) {

					$("#modalNewTask").modal("hide")

					Swal.fire({
						title: 'Genial!',
						text: 'Tarea guardada',
						icon: 'success',
						confirmButtonText: 'Aceptar'
					})
	
				//   $("#exampleModal").modal('hide')
	
				//   enabledButton('SaveButton')	
				})
				.catch(function(error) {
				Swal.fire({
					title: 'Upps!',
					text: 'Hubo un error al general la tarea',
					icon: 'error',
					confirmButtonText: 'Reintentar'
				})
				//   $("#exampleModal").modal('hide')
				//   enabledButton('SaveButton')
				});

				this.optionalDate=''
				this.specialTask=false
				this.taskName = ''
			}
		},
		
		getSpecialTask(){

			// setInterval(function(){
				db.collection("tasks").where('idUser', '==', localStorage.getItem("userEmail")).where('specialTask','==',true).onSnapshot((querySnapshot) => {

					if (!querySnapshot.empty) {
					 querySnapshot.forEach((doc) => {
						
						let dateTask = doc.data().taskDate;
						let taskStatus = doc.data().taskStatus;
	
						// let  yesterday = moment().subtract(1, "days").format("DD/MM/YYYY");
						let today = moment().format('YYYY-MM-DD');
	
						// let hour = moment().hour()+":"+moment().minute();
						let hour = moment().hour();
	
						if ((dateTask <= today || dateTask >= today) && taskStatus=='0') {//Solo hace el proceso un día antes o el día de la tarea
	
							// Validamos la hora
							switch (hour) {
								case 1:
									Push.create(doc.data().taskName, {
										body: "Hola, tienes una tarea importante pendiente para el "+dateTask,
										icon: '/assets/images/logoget.png',
										timeout: 9000,
										onClick: function () {
											window.focus();
											this.close();
										}
									});
								break;

								case 3:
									Push.create(doc.data().taskName, {
										body: "Hola, tienes una tarea importante pendiente para el "+dateTask,
										icon: '/assets/images/logoget.png',
										timeout: 9000,
										onClick: function () {
											window.focus();
											this.close();
										}
									});
								break;

								case 6:
									Push.create(doc.data().taskName, {
										body: "Hola, tienes una tarea importante pendiente para el "+dateTask,
										icon: '/assets/images/logoget.png',
										timeout: 9000,
										onClick: function () {
											window.focus();
											this.close();
										}
									});
								break;

								case 9:
									Push.create(doc.data().taskName, {
										body: "Hola, tienes una tarea importante pendiente para el "+dateTask,
										icon: '/assets/images/logoget.png',
										timeout: 9000,
										onClick: function () {
											window.focus();
											this.close();
										}
									});
								break;

								case 12:
									Push.create(doc.data().taskName, {
										body: "Hola, tienes una tarea importante pendiente para el "+dateTask,
										icon: '/assets/images/logoget.png',
										timeout: 9000,
										onClick: function () {
											window.focus();
											this.close();
										}
									});
								break;

								case 15:
									Push.create(doc.data().taskName, {
										body: "Hola, tienes una tarea importante pendiente para el "+dateTask,
										icon: '/assets/images/logoget.png',
										timeout: 9000,
										onClick: function () {
											window.focus();
											this.close();
										}
									});
								break;

								case 18:
									Push.create(doc.data().taskName, {
										body: "Hola, tienes una tarea importante pendiente para el "+dateTask,
										icon: '/assets/images/logoget.png',
										timeout: 9000,
										onClick: function () {
											window.focus();
											this.close();
										}
									});
								break;

								case 21:
									Push.create(doc.data().taskName, {
										body: "Hola, tienes una tarea importante pendiente para el "+dateTask,
										icon: '/assets/images/logoget.png',
										timeout: 9000,
										onClick: function () {
											window.focus();
											this.close();
										}
									});
								break;

								

								// case 21:
								// 	Push.create('Hello World!')
								// break;
							}
	
							// Push.create("Hello world!", {
							// 	body: "How's it hangin'?",
							// 	icon: '/icon.png',
							// 	timeout: 4000,
							// 	onClick: function () {
							// 		window.focus();
							// 		this.close();
							// 	}
							// });
						}
	
						// let startdate = moment(dateTask).format('DD-MM-YYYY');
						// startdate.subtract(1, 'd');
	
					 });
					}else{
						console.log("No hay tareas especial")
					}
				})
			// },450000)

			
		},

		getTagsOnSelect(){
			// Pendientes
			db.collection("tags").where('idUser', '==', localStorage.getItem("userEmail")).onSnapshot((querySnapshot) => {

				if (!querySnapshot.empty) {
					let e = $("#SelectTags").empty();
					e.append('<option>Seleccionar Etiqueta</option>')
				 querySnapshot.forEach((doc) => {
					e.append('<option value="'+doc.data().tagId+'">'+doc.data().tagName+'</option>')
				 });
				//  $("#totalPending").html(count)
				}else{
					// $("#totalPending").html(0)
				}
			})

		}
	},

	template:`
		<div>

			<div class="page-header">
	          <h3 class="page-title">
	            <span class="page-title-icon bg-gradient-info text-white mr-2">
	              <i class="mdi mdi-home"></i>
	            </span> Inicio
	          </h3>
	        </div>


	        <div class="row">
	          <div class="col-md-6 stretch-card grid-margin">
	            <div class="card bg-gradient-info card-img-holder text-white">
	              <div class="card-body">
	                <img src="assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
	                <h4 class="font-weight-normal mb-3">Tareas en proceso <i class="mdi mdi-bookmark-outline mdi-24px float-right"></i>
	                </h4>
	                <h2 class="mb-5" id="totalPending"></h2>
	                <h6 class="card-text" id="calcPending">No sé que wea poner</h6>
	              </div>
	            </div>
	          </div>
	          <div class="col-md-6 stretch-card grid-margin">
	            <div class="card bg-gradient-success card-img-holder text-white">
	              <div class="card-body">
	                <img src="assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
	                <h4 class="font-weight-normal mb-3">Tareas finalizadas <i class="mdi mdi-diamond mdi-24px float-right"></i>
	                </h4>
	                <h2 class="mb-5" id="totalDone"></h2>
	                <h6 class="card-text" id="calcDone">Lo mismo de la izquierda</h6>
	              </div>
	            </div>
	          </div>
	        </div>

	        <div class="row">
	          <div class="col-md-12 grid-margin stretch-card">
	            <div class="card">
	              <div class="card-body">
					<div class="alert alert-info" role="alert"><strong>TEN EN CUENTA!</strong> <br>
						-Pon la fecha si se trata de una tarea antigua o futura. <br>
						-Las tareas especiales se notificarán cada 3 horas, todos los días hasta el día de vencimiento y se distinguen por su color dorado, recuerda permitir mostrar notificaciones para este sitio.
					</div></h4>
	                <div class="row">
				</div>
	                <div class="list-wrapper">
					  <br>
	                  <ul class="d-flex flex-column-reverse todo-list todo-list-custom" id="taskList">
	                  </ul>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	        
	        <div class="row">
	          <div class="col-12 grid-margin">
	            <div class="card">
	              <div class="card-body">
	                <h4 class="card-title">Todas Mis tareas</h4>
	                <div class="table-responsive">
	                  <table class="display responsive nowrap" id="tablaTareas">
	                    <thead>
	                      <tr>
	                        <th class="all"> Tarea </th>
	                        <th class="none"> Estado </th>
	                        <th class="none"> Fecha de Inicio </th>
	                        <th class="none"> Fecha de Finalización </th>
	                        <th class="all"> Acciones </th>
	                      </tr>
	                    </thead>
	                    <tbody id="body-table">
	                      
	                    </tbody>
	                  </table>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>

			<div class="modal fade" id="modalNewTask" tabindex="-1" role="dialog" aria-labelledby="modalNewTaskLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="modalNewTaskLabel">Nueva Tarea</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12 mx-auto">
								<div>
									<label>Nombre de la Tarea</label>
									<input type="text" class="form-control todo-list-input" placeholder="¡Cuéntame qué harás!" v-model="taskName"><br>
								</div>
							</div>

							<div class="col-md-12 mx-auto">
								<div>
									<label>Fecha de Inicio</label>
									<input type="date" class="form-control date-list-input" v-model="optionalDate"><br>
								</div>
							</div>


							<div class="col-md-6 mx-auto">
								<div class="form-group">
										<select class="form-control" id="SelectTags" v-model="taskTag">
											<option>Seleccionar Etiqueta</option>
										</select>
								</div>
							</div>
							<div class="col-md-6 mx-auto">
								<div>
									<div class="form-check">
										<label class="form-check-label">
										<input type="checkbox" class="form-check-input" name="membershipRadios" id="membershipRadios1" v-model="specialTask">Tarea especial<i class="input-helper"></i></label>
									</div>
								</div>
							</div>
							<div class="col-md-12 mx-auto">
								<div>
									<button class="add btn btn-block btn-gradient-info font-weight-bold todo-list-add-btn" id="add-task" v-on:click="addTask">Añadir</button>
								</div>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="modalCheckTask" tabindex="-1" role="dialog" aria-labelledby="modalCheckTaskLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="modalCheckTaskLabel">Marcar Tarea</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<label>Marcar fecha de finalización</label>
						<input type="date" class="form-control" id="check-date"><br>
						<div class="col-md-12 mx-auto">
							<div>
								<input type="hidden" id="status-data"></input>
								<input type="hidden" id="status-id"></input>
								<button class="add btn btn-block btn-gradient-info font-weight-bold" onclick="checkTaskwithDate()">Marcar Tarea</button>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="ModalEditLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="ModalEditLabel">Editar Tarea</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12 mx-auto">
								<div>
									<label>Nombre de la Tarea</label>
									<input type="text" class="form-control todo-list-input" placeholder="¡Cuéntame qué harás!" id="editTaskName"><br>
								</div>
							</div>

							<div class="col-md-12 mx-auto">
								<div>
									<label>Fecha de Inicio</label>
									<input type="date" class="form-control date-list-input" id="editOptionalDate"><br>
								</div>
							</div>

							<div class="col-md-12 mx-auto">
								<div class="form-group">
									<select class="form-control" id="SelectTags2">
										<option>Seleccionar Etiqueta</option>
									</select>
								</div>
							</div>

							<div class="col-md-12 mx-auto">
								<div>
									<input type="hidden" id="idEdit"><br>
									<button class="add btn btn-block btn-gradient-info font-weight-bold" onclick="editTaskAction()">Guardar</button>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
					</div>
				</div>
			</div>

		</div>
	`
})