new Vue({

	el:'#login',

	data(){
		return{
			email:'',
            pw:''
		}
	},

	methods:{
        login(){
            login(this.email,this.pw)
        }
    },

    mounted() {
        auth.onAuthStateChanged(user => {
            if (user) {
            	localStorage.setItem('userEmail',user.email)
            	localStorage.setItem('UserdisplayName',user.displayName)
            	localStorage.setItem('UserphotoURL',user.photoURL)
        		window.location.assign('app.html')
            }
        })   
    },

	template:`
		<div class="container-scroller">
		  	<div class="container-fluid page-body-wrapper full-page-wrapper">
			    <div class="content-wrapper d-flex align-items-center auth">
			      <div class="row flex-grow">
			        <div class="col-lg-4 mx-auto">
			          <div class="auth-form-light text-left p-5">
			            <div class="brand-logo">
			              <img src="assets/images/logoget.png">
			            </div>
			            <h4>Hola, vamos a organizar tu día xd</h4>
			            <h6 class="font-weight-light">Inicia Sesión para continuar</h6>
			            <form>
			              <div class="form-group">
			                <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Correo" v-model='email'>
			              </div>
			              <div class="form-group">
			                <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Contraseña" v-model='pw'>
			              </div>
			              <div class="mt-3">
			                <a class="btn btn-block btn-gradient-info btn-lg font-weight-medium auth-form-btn" href="#" v-on:click.prevent="login">INGRESAR</a>
			                <a class="btn btn-block btn-gradient-danger btn-lg font-weight-medium auth-form-btn" href="#" onclick="loginGoogle()">SignIn with google</a>
			              </div>
			            </form>
			          </div>
			        </div>
			      </div>
			    </div>
		  	</div>
		</div>
	`
})