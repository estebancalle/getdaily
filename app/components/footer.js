new Vue({
	el:'#footer',

	data(){
		return{
			version:1.1
		}
	},

	template:`
		<footer class="footer">
	        <div class="container-fluid clearfix">
	          <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright GETDAILY! V{{version}}</span>
	        </div>
	    </footer>
	`
})